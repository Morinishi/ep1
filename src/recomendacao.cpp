#include "recomendacao.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <fstream>
#include <stdbool.h>

Recomendacao::Recomendacao(){
}

Recomendacao::Recomendacao(vector <Produto*> lista){
    carrega_arquivo();
    carrega_lista_produtos(lista);
    set_tcategorias();
}
Recomendacao::~Recomendacao(){}

void Recomendacao::set_tcategorias(){
    this->t_categorias=int(this->categorias.size());
}

void Recomendacao::concatena(string cpf){
    string txt,final=".txt";
    txt=cpf+final;
    this->nome=txt;
}

void Recomendacao::carrega_lista_produtos(vector <Produto*> lista){
    this->lista=lista;
}

void Recomendacao::retorna_categorias_lista(){
}


void Recomendacao::fecha_arquivo(){
    ofstream arquivo;
    arquivo.open("arquivos/"+this->nome);
    for (int i=0;i<this->t_categorias;i++){
        cout<<categorias[i]<<endl;
        arquivo<<conta[i]<<endl;
    }
    for(int i=0;i<this->t_categorias;i++){
        arquivo<<categorias[i]<<endl;
        arquivo<<conta[i]<<endl;
    }    
    arquivo.close();
}


void Recomendacao::carrega_arquivo(){
    if(verifica_arquivo()==true){
        string categoria,tamanho,quantidade;
        ifstream arquivo;
        int t_tamanho,quant;
        arquivo.open("arquivos/"+this->nome);
        getline(arquivo,tamanho);
        t_tamanho=atoi(tamanho.c_str());
        for(int i=0;i<t_tamanho;i++){
            vector <string> categorias;
            getline(arquivo,categoria);
            getline(arquivo,quantidade);
            quant=atoi(quantidade.c_str());
            this->categorias.push_back(categoria);
            this->conta.push_back(quant);
        }
        arquivo.close();
    }
    if(verifica_arquivo()==false){
    }
}


bool Recomendacao::verifica_arquivo(){
    ifstream arquivo;
    arquivo.open("arquivos/"+this->nome);
    if(arquivo.is_open()){
        arquivo.close();
        return true;
    }
    else{
        return false;
    }
}

