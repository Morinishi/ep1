#include "carrinho.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <stdbool.h>


Carrinho::Carrinho(){
    set_preco();
    set_t_estoque();
    set_estoque();
    set_t_produtos_list();
}


Carrinho::~Carrinho(){}


void Carrinho::set_t_produtos_list(){
    this->t_produtos_list=this->produtos_list.size();
}


int Carrinho::get_t_produtos_list(){
    return this->t_produtos_list;
}


void Carrinho::get_produtos_list(){
    for(int i=0;i<this->t_produtos_list;i++){
        cout<<"Nome: "<<this->produtos_list[i]->get_nome()<<endl;
        cout<<"Quantidade: "<<this->produtos_list[i]->get_quantidade()<<endl;
        cout<<endl<<"-----------------------------------"<<endl;
    }
}

vector <Produto*> Carrinho::return_produtos_list(){
    return this->produtos_list;
}


double Carrinho::get_preco(){
    return this->preco;
}


string Carrinho::get_pagamento(){
    return this->pagamento;
}

void Carrinho::set_preco(){
    if(this->t_produtos_list!=0){
        this->preco=0;   
        for(int i=0;i<this->t_produtos_list;i++){
            this->preco+=this->produtos_list[i]->get_valor()*this->produtos_list[i]->get_quantidade();
        }
    }else{
        this->preco=0;
    }
}

void Carrinho::set_pagamento(){
    string pagamento;
    cout<<"Qual a forma de pagamento?"<<endl;
    cin.ignore();
    getline(cin,pagamento);
    this->pagamento=pagamento;
}

void Carrinho::cadastra_produto(){
    string nome;
    cout<<"Digite o nome do produto:";
    cin.ignore();
    getline(cin,nome);
    for(int i=0;i<this->t_produtos_list;i++){
        if(nome==this->produtos_list[i]->get_nome()){
            this->produtos_list[i]->set_quantidade(this->produtos_list[i]->get_quantidade()+1);
            return;
        }
    }
    Produto *meu_produto;
    meu_produto=get_produto_estoque(nome);
    if(meu_produto==NULL){
        return;
    }
    meu_produto->set_quantidade(1);
    if(meu_produto!= NULL){
        this->produtos_list.push_back(meu_produto);
        this->t_produtos_list++;
        cout<<"Produto cadastrado"<<endl;
    }
}

void Carrinho::rmv_prod(){
    string nome;
    cout<<"Digite o nome do produto: ";
    cin.ignore();
    getline(cin,nome);
    for(int i=0;i<this->t_produtos_list;i++){
        if(this->produtos_list[i]->get_nome()==nome){
            this->produtos_list.erase(this->produtos_list.begin()+i );
            this->t_produtos_list--;
            cout<<"Produto removido!"<<endl;
            return;
        }
    }
    
    cout<<"Não há o produto no carrinho!"<<endl;
    
}


bool Carrinho::realizar_pagamento(){
    int opcao;
    set_pagamento();
    set_preco();
    system("clear");
    cout<<"Lista de produtos e seus respectivos preços e quantidades:"<<endl<<endl;
    for(int i=0;i<this->t_produtos_list;i++){
        cout<<this->produtos_list[i]->get_nome()<<" -- R$ "<<this->produtos_list[i]->get_valor()<<" -- "<<this->produtos_list[i]->get_quantidade()<<endl;
    }
    cout<<endl<<"Valor total dos produtos: R$ "<<get_preco()<<endl;
    cout<<"Valor do desconto oferecido: R$ "<<get_preco()*0.15<<endl;
    cout<<"Valor final da venda: R$ "<<get_preco()-(get_preco()*0.15)<<endl;
    cout<<"Forma de pagamento: "<<get_pagamento()<<endl<<endl;
    cout<<"Deseja confirmar a operação?(1 sim 0 não)"<<endl;
    cin>>opcao;
    if(opcao==1){
        Produto *meu_produto;
        for(int i=0;i<this->t_produtos_list;i++){
            meu_produto=get_produto_estoque(this->produtos_list[i]->get_nome());
            if(meu_produto->get_quantidade()<this->produtos_list[i]->get_quantidade()){
                cout<<"Erro, quantidade da compra maior que do estoque."<<endl;
                cin.ignore();
                getchar();
                return true;
            }
        }
        for(int i=0;i<this->t_produtos_list;i++){
            altera_quantidade(this->produtos_list[i]->get_nome(),this->produtos_list[i]->get_quantidade());
        }
        cout<<"Operação concluida!"<<endl;
        getchar();
        return true;
    }else{
        return false;
    }
}