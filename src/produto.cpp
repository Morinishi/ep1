#include "produto.hpp"
#include <iostream>
#include <string>


Produto::Produto(){
    set_quantidade(0);
    set_codigo("");
    set_valor(0);
    set_nome("");
}


Produto::Produto(string nome,vector<string> categorias,double valor,int quantidade,string codigo){
    set_quantidade(quantidade);
    set_codigo(codigo);
    set_valor(valor);
    set_nome(nome);
    set_categorias(categorias);
}


Produto::~Produto(){}


int Produto::get_quantidade(){
    return this->quantidade;
}

string Produto::get_codigo(){
    return this->codigo;
}

double Produto::get_valor(){
    return this->valor;
}

string Produto::get_nome(){
    return this->nome;
}

string Produto::get_categorias(int indice){
    return this->categorias[indice];
}

vector <string> Produto::get_vector_categorias(){
    return this->categorias;
}


void Produto::set_quantidade(int quantidade){
    this->quantidade=quantidade;
}

void Produto::set_codigo(string codigo){
    this->codigo=codigo;
}

void Produto::set_valor(double valor){
    this->valor=valor;
}

void Produto::set_nome(string nome){
    this->nome=nome;
}

void Produto::set_categorias(vector <string> categorias){
    this->categorias=categorias;
}

int Produto::get_t_categorias(){
    return this->t_categorias;
}
void Produto::set_t_categorias(){
    this->t_categorias=int(categorias.size());
}