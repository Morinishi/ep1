#include "estoque.hpp"
#include "produto.hpp"

#include <vector>
#include <iostream>
#include <stdbool.h>
#include <fstream>
#include <string>


Estoque::Estoque(){
    set_t_estoque();
    set_estoque();
    set_t_categoria();
    carrega_categorias();
}


Estoque::~Estoque(){}


int Estoque::get_t_estoque(){
    return this->t_estoque;
}


void Estoque::get_estoque_de_produtos(){
    for(int i=0;i<this->t_estoque;i++){
        cout<<"Nome: "<<this->estoque_de_produtos[i]->get_nome()<<endl;
        cout<<"Categorias: ";
        for(int j=0;j<this->estoque_de_produtos[i]->get_t_categorias();j++){
                cout<<this->estoque_de_produtos[i]->get_categorias(j)<<"|";
        }
        cout<<endl<<"Valor: R$"<<this->estoque_de_produtos[i]->get_valor()<<endl;
        cout<<"Quantidade: "<<this->estoque_de_produtos[i]->get_quantidade()<<endl;
        cout<<"Código: "<<this->estoque_de_produtos[i]->get_codigo()<<endl;
        cout<<"----------------------------------------------------------------"<<endl;
    }
}


Produto* Estoque::get_produto_estoque(string nome){
    for(int i=0;i<this->t_estoque;i++){
        if(nome==this->estoque_de_produtos[i]->get_nome()){
            Produto*meu_produto=new Produto(this->estoque_de_produtos[i]->get_nome(),this->estoque_de_produtos[i]->get_vector_categorias(),this->estoque_de_produtos[i]->get_valor(),this->estoque_de_produtos[i]->get_quantidade(),this->estoque_de_produtos[i]->get_codigo());
            return meu_produto;
        }
    }
    cout<<"Não existe esse produto no estoque!"<<endl;
    return NULL;
}


void Estoque::set_t_estoque(){
    ifstream estoque;
    string t_estoque;
    estoque.open("arquivos/estoque.txt",ios::in);
    if(estoque.is_open()){
        getline(estoque,t_estoque);
        this->t_estoque=atoi(t_estoque.c_str());
        estoque.close();
    }else{
        this->t_estoque=0;
    }
}


void Estoque::set_estoque(){
    if(verifica_estoque()==true){
        string nome, categoria,valor,quantidade,codigo,lixo,t_categorias;
        ifstream estoque;
        int quant,t_categoria;
        double value;
        estoque.open("arquivos/estoque.txt",ios::in);    
        getline(estoque,lixo);
        Produto* meu_produto;
        for(int i=0;i<this->t_estoque;i++){
            vector <string> categorias;
            getline(estoque,nome);
            getline(estoque,t_categorias);
            t_categoria=atoi(t_categorias.c_str());
            for(int j=0;j<t_categoria;j++){
                getline(estoque,categoria);
                categorias.push_back(categoria);
            }
            getline(estoque,valor);
            getline(estoque,quantidade);
            getline(estoque,codigo);
            quant=atoi(quantidade.c_str());
            value=atof(valor.c_str());
            meu_produto=new Produto(nome,categorias,value,quant,codigo);
            meu_produto->set_t_categorias();
            this->estoque_de_produtos.push_back(meu_produto);
        }
        estoque.close();
    }
    if(verifica_estoque()==false){
    }
}


bool Estoque::verifica_estoque(){
    ifstream estoque;
    estoque.open("arquivos/estoque.txt",ios::in);
    if(estoque.is_open()){
        estoque.close();
        return true;
    }
    else{
        return false;
    }
}


void Estoque::cadastra_produto(){

    vector <string> categorias;
    string nome,categoria,codigo;
    int quantidade,t_categorias,contador=0;
    double valor;


    cout<<"Digite o nome do produto:";
    cin.ignore();
    getline(cin,nome);
    for(int i=0;i<this->t_estoque;i++){
        if(nome==this->estoque_de_produtos[i]->get_nome()){
            cout<<"Produto já existe.";
            return;
        }
    }


    cout<<endl<<"Produto não encontrado. Iniciando cadastro..."<<endl;
    cout<<endl<<"Qual o número de categorias que o produto terá?";
    cin>>t_categorias;
    cin.ignore();
    for(int i=0;i<t_categorias;i++){
        cout<<endl<<"Digite a categoria:";
        getline(cin,categoria);
        categorias.push_back(categoria);
    }
    cout<<endl<<"Codigo:";
    getline(cin,codigo);
    cout<<endl<<"Valor:";
    cin>>valor;
    cout<<endl<<"Quantidade:";
    cin.ignore();
    cin>>quantidade;
    

    for(int i=0;i<this->t_categoria;i++){
        for(int j=0;j<t_categorias;j++){
            if(categorias[j]==this->categoria[i]){
                contador++;
            }
        }
    }
    if(contador==t_categorias){
        Produto*meu_produto=new Produto(nome,categorias,valor,quantidade,codigo);
        meu_produto->set_t_categorias();
        this->estoque_de_produtos.push_back(meu_produto);
        this->t_estoque++;
        cout<<"Produto cadastrado com sucesso!"<<endl;
        return;
    }
    cout<<"Categoria não existente!"<<endl;
}


void Estoque::fechar_estoque(){
    if(verifica_estoque()==true||this->estoque_de_produtos.size()!=0){
        ofstream estoque;
        estoque.open("arquivos/estoque.txt",ios::out);
        estoque<<this->t_estoque<<endl;
        for(int i=0;i<this->t_estoque;i++){
            estoque<<this->estoque_de_produtos[i]->get_nome()<<endl;
            estoque<<this->estoque_de_produtos[i]->get_t_categorias()<<endl;
            for(int j=0;j<this->estoque_de_produtos[i]->get_t_categorias();j++){
                estoque<<this->estoque_de_produtos[i]->get_categorias(j)<<endl;
            }
            estoque<<this->estoque_de_produtos[i]->get_valor()<<endl;
            estoque<<this->estoque_de_produtos[i]->get_quantidade()<<endl;
            estoque<<this->estoque_de_produtos[i]->get_codigo()<<endl;
        }
        estoque.close();
    }else{
    }
}


void Estoque::fechar_categorias(){
    if(verifica_categorias()==true||categoria.size()!=0){
        ofstream categorias;
        categorias.open("arquivos/categorias.txt",ios::out);
        categorias<<this->t_categoria<<endl;
        for(int i=0;i<this->t_categoria;i++){
            categorias<<this->categoria[i]<<endl;
        }
        categorias.close();

    }else{
    }
}


bool Estoque::verifica_categorias(){
    ifstream categorias;
    categorias.open("arquivos/categorias.txt",ios::in);
    if(categorias.is_open()){
        categorias.close();
        return true;
    }
    else{
        return false;
    }
}


void Estoque::carrega_categorias(){
    if(verifica_categorias()==true){
        string categoria,lixo;
        ifstream categorias;
        categorias.open("arquivos/categorias.txt",ios::in);
        getline(categorias,lixo);
        for(int i=0;i<this->t_categoria;i++){
            getline(categorias,categoria);
            this->categoria.push_back(categoria);
        }
        categorias.close();
    }
    if(verifica_categorias()==false){
    }
}


void Estoque::get_categorias(){
    for(int i=0;i<this->t_categoria;i++){
        cout<<this->categoria[i]<<endl;
    }
}


void Estoque::set_categoria(){
    string categoria;
    cout<<"Qual o nome da categoria que quer adicionar?"<<endl;
    cin.ignore();
    getline(cin,categoria);
    this->categoria.push_back(categoria);
    cout<<"Categoria adicionada!"<<endl;
    this->t_categoria++;
}


int Estoque::get_t_categoria(){
    return this->t_categoria;
}


void Estoque::set_t_categoria(){
    ifstream categorias;
    string t_categorias;
    categorias.open("arquivos/categorias.txt",ios::in);
    if(categorias.is_open()){
        getline(categorias,t_categorias);
        this->t_categoria=atoi(t_categorias.c_str());
        categorias.close();
    }else{
        this->t_categoria=0;
    }
}

void Estoque::altera_quantidade(){
    string nome;
    int quantidade;
    cout<<"Digite o nome do produto:";
    cin.ignore();
    getline(cin,nome);
    for(int i=0;i<this->t_estoque;i++){
        if(nome==this->estoque_de_produtos[i]->get_nome()){
            cout<<"Digite a nova quantidade:";
            cin>>quantidade;
            this->estoque_de_produtos[i]->set_quantidade(quantidade);
            cout<<"Nova quantidade registrada!"<<endl;
            return;
        }
    }
    cout<<"Produto não encontrado."<<endl;
}

void Estoque::altera_quantidade(string nome,int quantidade){
    for(int i=0;i<this->t_estoque;i++){
        if(nome==this->estoque_de_produtos[i]->get_nome()){
            this->estoque_de_produtos[i]->set_quantidade(estoque_de_produtos[i]->get_quantidade()-quantidade);
            return;
        }
    }
}