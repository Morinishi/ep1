#include <iostream>
#include "cliente.hpp"
#include "socio.hpp"
#include <vector>
#include <string>
#include "carrinho.hpp"
#include <fstream>
#include "estoque.hpp"
#include "produto.hpp"
#include "recomendacao.hpp"

using namespace std;

int main(){

  
  int decisao=0;
  char escolha,op;
  string nome,email,sexo,endereco,cpf,nasc,telefone;
  bool final=true;
  Socio* meu_socio=new Socio();
  do{

  
    

  
    system("clear");
    cout<<"Bem vindo a loja de magicas"<<endl;
    cout<<"1. Modo Venda."<<endl;
    cout<<"2. Modo Estoque."<<endl;
    cout<<"3. Modo Recomendação."<<endl;
    cout<<"0. Fechar programa."<<endl;
    
  
    cin>>op;
    cin.ignore();
    system("clear");

  
    switch(op){
  
    case '1':
      {
      meu_socio->cadastra_socio();
      getchar();

      Carrinho *meu_carrinho=new Carrinho();


      do{
        system("clear");
        cout<<"BEM VINDO AO MENU DE VENDA!"<<endl;
        cout<<"1. Colocar produto no carrinho."<<endl;
        cout<<"2. Retirar produto do carrinho."<<endl;
        cout<<"3. Ver carrinho."<<endl;
        cout<<"4. Ver estoque de produtos."<<endl;
        cout<<"5. Finalizar operação"<<endl;
        cout<<"0. Voltar ao menu."<<endl;

        cin>>escolha;
        system("clear");
        switch(escolha){
        case '1':
          {
          meu_carrinho->cadastra_produto();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '2':
          {
          meu_carrinho->rmv_prod();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '3':
          {
          meu_carrinho->get_produtos_list();
          cin.ignore();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '4':
          {
          meu_carrinho->get_estoque_de_produtos();
          cin.ignore();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '5':
          {
          final=meu_carrinho->realizar_pagamento();
          if(final==false){
            decisao=0;
          }else{
            decisao=1;
            getchar();
          }
          break;
          }
        case '0':
          {
          break;
          }
        default:
          {
          cout<<"Opção inválida! Aperte enter para retornar."<<endl;
          cin.ignore();
          getchar();
          break;
          }
        }

      
      }while(escolha!='0' && decisao==0);
      meu_carrinho->fechar_estoque();
      meu_carrinho->fechar_categorias(); 
      break;
      }
  
    case '2': 
      {
  
      Estoque *estoque_de_produtos=new Estoque();
  
      do{
        system("clear");
  
        cout<<"MENU DE ESTOQUE!"<<endl;
        cout<<"1. Cadastrar produto."<<endl;
        cout<<"2. Cadastrar categoria."<<endl;
        cout<<"3. Ver estoque."<<endl;
        cout<<"4. Ver categorias."<<endl;
        cout<<"5. Alterar quantidade de um produto"<<endl;
        cout<<"0. Voltar ao menu."<<endl;
  
        cin>>escolha; 
  
        system("clear");
        switch(escolha){
        case '1':
          {
  
          estoque_de_produtos->cadastra_produto();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          cin.ignore();
          getchar();
          break;
          }
        case '2':
          {
  
          estoque_de_produtos->set_categoria();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '3':
          {
  
          estoque_de_produtos->get_estoque_de_produtos();
          cin.ignore();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '4':
          {
  
          estoque_de_produtos->get_categorias();
          cin.ignore();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '5':
          {
          estoque_de_produtos->altera_quantidade();
          cout<<endl<<"Aperte enter para continuar..."<<endl;
          getchar();
          break;
          }
        case '0':
          {
          break;
          }
        default:
          {
          cout<<"Opção inválida! Aperte enter para retornar."<<endl;
          cin.ignore();
          getchar();
          break;
          }
        }

      }while(escolha!='0');
  
      estoque_de_produtos->fechar_estoque();
      estoque_de_produtos->fechar_categorias(); 
      
      break;
      }
  
    case '3':
      {
      break;
      }
    case '0':
      {
      meu_socio->fechar_socio();
      return 0;
      }
    default:
      {
      cout<<"Opção inválida!"<<endl<<"Aperte qualquer tecla para voltar ao menu."<<endl;
      getchar();
      break;
      }
    }

  }while(op!='0');
  return 0;
}

