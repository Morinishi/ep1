#ifndef recomendacao_hpp
#define recomendacao_hpp

#include "produto.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <stdbool.h>



class Recomendacao{
private:
    int t_categorias;
    string nome;
    vector <Produto*> lista;
    vector <string> categorias;
    vector <int> conta;
public:
    Recomendacao();
    Recomendacao(vector <Produto*> lista);
    ~Recomendacao();
    void concatena(string nome);
    void carrega_lista_produtos(vector <Produto*> lista);
    void set_tcategorias();
    void fecha_arquivo();
    void carrega_arquivo();
    bool verifica_arquivo();
    void retorna_categorias_lista();
};


#endif