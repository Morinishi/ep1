#ifndef estoque_hpp
#define estoque_hpp


#include "produto.hpp"


#include <iostream>
#include <vector>
#include <stdbool.h>


class Estoque{
private:
    int t_estoque,t_categoria;
    vector<Produto*>estoque_de_produtos;
    vector<string> categoria;
public:

    Estoque();

    ~Estoque();

    int get_t_categoria();
    int get_t_estoque();
    void get_estoque_de_produtos();
    Produto* get_produto_estoque(string nome);
    void get_categorias();
    void set_t_estoque();
    void set_estoque();
    void set_categoria();
    void set_t_categoria();
    
    bool verifica_estoque();
    virtual void cadastra_produto();
    void fechar_estoque();
    bool verifica_categorias();
    void carrega_categorias();
    void fechar_categorias();
    void altera_quantidade();
    void altera_quantidade(string nome,int quantidade);

};

#endif