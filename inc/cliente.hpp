#ifndef cliente_hpp
#define cliente_hpp


#include <iostream>
#include <string>

using namespace std;


class Cliente{
private:
  string nome,email,cpf;
public:


  Cliente();

  ~Cliente();



  string get_nome();
  string get_email();
  string get_cpf();

  void set_nome(string nome);
  void set_email(string email);
  void set_cpf(string cpf);

};

#endif