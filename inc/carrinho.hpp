#ifndef CARRINHO_HPP
#define CARRINHO_HPP
#include "produto.hpp"
#include "estoque.hpp"
#include <vector>
#include <iostream>
#include <string>
#include <stdbool.h>

using namespace std;

class Carrinho:public Estoque{
private:
    vector <Produto*> produtos_list;
    double preco; 
    string pagamento; 
    int t_produtos_list; 
public:

    
    Carrinho();

    ~Carrinho();
    void get_produtos_list();
    vector <Produto*> return_produtos_list();
    double get_preco();
    string get_pagamento();
    int get_t_produtos_list();

    void set_preco();
    void set_pagamento();
    void set_t_produtos_list();
    

    
    void cadastra_produto();
    void rmv_prod();
    bool realizar_pagamento();
};

#endif